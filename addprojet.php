<?php 


include("model.php");

// ------------------------------Formulaire ajout projet

$listPreProjet=getAllPreProjet();

$titre = $_POST["p-titre"];
$image = $_POST["p-image"];
$description = $_POST["p-description"];
$date = $_POST["p-date"];
$lien = $_POST["p-lien"];



//--------------------------------------Competences


$c1 = $_POST["p-comp1"];
$c2 = $_POST["p-comp2"];
$c3 = $_POST["p-comp3"];
$c4 = $_POST["p-comp4"];
$c5 = $_POST["p-comp5"];
$c6 = $_POST["p-comp6"];
$c7 = $_POST["p-comp7"];
$c8 = $_POST["p-comp8"];
$comps = [$c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8];



// ----------------------------------------Techno



$listTechno=getAllTechno();
$idTechno = [];
foreach ($listTechno as $i_tech){
    if(ISSET($_POST[$i_tech["nom"]])){
         $idTechno[] = $i_tech["id"];
    };
}

// -----------------------------------------Fonction ajout projet



addProjet($titre, $image, $description, $date, $lien, $idTechno, $comps);

header('Location:formulaire.php');
?>