<?php include 'header.php'; 
      include 'model.php';
      

      $listTechno= getAllTechno();
     

      //--------------------------FORMULAIRE AJOUT PROJETS----------------------------------------------------------------


      
    
      ?>
        <form class="formu" id="container" action="addprojet.php" autocomplete="off" method="post" >
            <input type="text" name="p-titre" id="p-titre" placeholder="Titre">

            <label for="p-date">Date du Projet</label>
            <input type="date" name="p-date" id="p-date"required>

            <input type="file" name="p-image" id="p-image"  accept="image/png, image/jpeg"required>

            <textarea name="p-description" id="p-description" placeholder="Description"required></textarea>
            <label for="p-lien">Lien du Projet</label>
            <input type="url" name="p-lien" id="p-lien" pattern="https://.*"required>


            <!-- --------------------------------Liste technos -->


            <span class="titreform">TECHNOS</span>

        <?php foreach ($listTechno as $ajtech){?>

            <input type="checkbox"  value=<?php echo $ajtech['id']?> name="<?php echo $ajtech['nom'];?>" id="<?php echo $ajtech['nom'];?>">
            <label for="tech-<?php echo $ajtech['nom'];?>"><?php echo $ajtech['nom'];?></label>
        <?php } ?>


            <!-- --------------------------------Liste compétences -->


            <span class="titreform">COMPÉTENCES</span>

            <input type="checkbox" name="p-comp1" value="1" id="p-comp1">
            <label for="p-comp1">C1</label>

            <input type="checkbox" name="p-comp2" value="2" id="p-comp2">
            <label for="p-comp2">C2</label>

            <input type="checkbox" name="p-comp3" value="3" id="p-comp3">
            <label for="p-comp3">C3</label>

            <input type="checkbox" name="p-comp4" value="4" id="p-comp4">
            <label for="p-comp4">C4</label>

            <input type="checkbox" name="p-comp5" value="5" id="p-comp5">
            <label for="p-comp5">C5</label>

            <input type="checkbox" name="p-comp6" value="6" id="p-comp6">
            <label for="p-comp6">C6</label>

            <input type="checkbox" name="p-comp7" value="7" id="p-comp7">
            <label for="p-comp7">C7</label>

            <input type="checkbox" name="p-comp8" value="8" id="p-comp8">
            <label for="p-comp8">C8</label>

            <input type="submit" value="Ajouter">
        </form>
    

    <!-- //--------------------------FORMULAIRE AJOUT TECHNOS---------------------------------------------------------------- -->
    

    <form class="formu" action="addtech.php" method="post" autocomplete="off">
            <label for="aj-tech">Ajouter une Technologie</label>
            <input type="text" name="aj-tech" id="aj-tech">
            
            <input type="submit" value="Ajouter">

        </form>
</body>
</html>