<?php include('header.php');
      include('model.php');
    $listPreProjet= getAllPreProjet(); ?>
    <!-- //-------------Partie About me-----------------// -->


    <div id="p-aboutme">
      <div class="en-tete">
        <div class="index-titre">
          <a href="index.php">
            <div class="titre1">CAULET<br />PortFolio'21</div>
          </a>
        </div>
        <div class="menu-nav">
          <a href="#p-projet" class="lien-Nav1"><div>Projets</div></a>
          <a href="#p-contact" class="lien-Nav1"><div>Contact</div></a>
        </div>
      </div>
      <video autoplay loop id="bgvid">
        <source src="./Asset/Codevid.mp4" type="video/mp4"/>
      </video>
      <div id="block-aboutme">
        <div id="titre-aboutme">
          <h1>Caulet Guillaume</h1>
          <div id="portfolio">PortFolio '21</div>
        </div>
        <div class="b-text-scroll">
          <div class="b-text">
            <span>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex vitae
              minus corporis libero sed aliquam blanditiis! Quis quaerat error,
              placeat dolorem sit vel et soluta?
              </span>
            <div>
              Lorem, ipsum.
            </div>
            <div>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fugiat,
              est!
            </div>
          </div>
          <div id="b-scroll">
            <div class="container">
              <a href="#p-projet">
                <div class="chevron"></div>
                <div class="chevron"></div>
                <div class="chevron"></div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- //-------------Partie Projets---------------// -->


    <div id="p-projet">
      <div class="en-tete">
        <div class="menu-titre">
          <a href="#p-aboutme">
            <div class="titre2">CAULET<br />PortFolio'21</div>
          </a>
        </div>
        <div class="menu-nav">
          <a href="#p-aboutme" class="lien-Nav2"><div>About Me</div></a>
          <a href="#p-contact" class="lien-Nav2"><div>Contact</div></a>
        </div>
      </div>
      <div id="block">
        <h2>Projets</h2>
      </div>
      <div>
      <?php foreach ($listPreProjet as $i){?>  
        <div class="projet" id="<?= $i['titre']?>"><a style="color:black;" href="projet.php"><?= $i['titre']?></a><?= $i['date_p']?></div><?php } ?>
        <!-- <div class="projet" id="P2"><a href="projet.php">XX2</a></div>
        <div class="projet" id="P3"><a href="projet.php">XX3</a></div>
        <div class="projet" id="P4"><a href="projet.php">XX4</a></div>
        <div class="projet" id="P5"><a href="projet.php">XX5</a></div> -->
      </div>
    </div>


    <!-- //-------------Partie Contact----------------// -->


    <div id="p-contact">
      <div class="en-tete">
          <div class="menu-titre">
            <a href="#p-aboutme">
              <div class="titre2">CAULET<br />PortFolio'21</div>
            </a>
          </div>
          <div class="menu-nav">
            <a href="#p-aboutme" class="lien-Nav2"><div>About Me</div></a>
            <a href="#p-projet" class="lien-Nav2"><div>Projets</div></a>
          </div>
        </div>
      <div id="block-contact">
        <div>
          <h2>Contact</h2>
        </div>
        <div class="l-contact">
          <div class="contact" id="C1"><a href="https://gitlab.com/" target="_blank" style="color:black;">GitLab</a></div>
          <div class="contact" id="C2"><a href="https://fr.linkedin.com/" target="_blank" style="color:black;">Linkedin</a></div>
          <div class="contact" id="C3"><a href="https://fr-fr.facebook.com/"target="_blank" style="color:black;">Facebook</a></div>
          <div class="contact" id="C4"><a href="https://www.instagram.com/?hl=fr"target="_blank" style="color:black;">Instagram</a></div>
        </div>
      </div>
    </div>
  </body>
  <script src="script.js"></script>
</html>
