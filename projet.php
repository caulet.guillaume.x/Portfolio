<?php include('header.php');
      include('model.php');
      $listPreProjet= getAllPreProjet();?>

<div id="p-aboutme">
      <div class="en-tete">
        <div class="index-titre">
          <a href="index.php">
            <div class="titre1">CAULET<br />PortFolio'21</div>
          </a>
        </div>
        <div class="menu-nav">
          <a href="index.php#p-projet" class="lien-Nav1"><div>Projets</div></a>
          <a href="index.php#p-contact" class="lien-Nav1"><div>Contact</div></a>
        </div>
      </div>
      <video autoplay loop id="bgvid2">
        <source src="./Asset/sincity.mp4" type="video/mp4"/>
      </video>
      <?php foreach ($listPreProjet as $i){?>
      <div id="block-aboutme">
        <div id="titre-aboutme">
          <h1><?= $i['titre']?></h1>
          <div id="portfolio">PortFolio '21</div>
        </div>
        <div class="b-text-scroll">
          <div class="b-text">
            <span id="Dc"><?= $i['desc_p']?>
              </span>
            <div id="Dt">
            <?= $i['date_p']?>
            </div>
            <div id="Li">
            <a style="color:white;" href="<?= $i['lien_p']?>"target="_blank"><?= $i['lien_p']?></a>
            </div>
          </div>
          <div id="b-scroll">
            <div class="container">
              <a href="">
                <div class="chevron"></div>
                <div class="chevron"></div>
                <div class="chevron"></div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div><?php } ?>
</body>
</html>
