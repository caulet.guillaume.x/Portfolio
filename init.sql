DROP DATABASE IF EXISTS portfolio;

CREATE DATABASE portfolio;

USE portfolio;

CREATE TABLE projet ( 
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    titre VARCHAR(30) NOT NULL,
    img VARCHAR(255) NOT NULL,
    desc_p VARCHAR(255) NOT NULL,
    date_p DATE NOT NULL,
    lien_p VARCHAR(255) NOT NULL
);

CREATE TABLE techno (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(40) NOT NULL);

CREATE TABLE competence (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(40) NOT NULL
);

CREATE TABLE p_techno (
    id_projet INT UNSIGNED,
    id_techno INT UNSIGNED,
    UNIQUE (id_projet, id_techno),
    FOREIGN KEY (id_projet) REFERENCES projet(id),
    FOREIGN KEY (id_techno) REFERENCES techno(id)
);

CREATE TABLE p_competence (
    id_projet INT UNSIGNED,
    id_comp INT UNSIGNED,
    UNIQUE (id_projet, id_comp),
    FOREIGN KEY (id_projet) REFERENCES projet(id),
    FOREIGN KEY (id_comp) REFERENCES competence(id)
);
INSERT INTO `competence` (`id`, `nom`) VALUES (NULL, 'C1');
INSERT INTO `competence` (`id`, `nom`) VALUES (NULL, 'C2');
INSERT INTO `competence` (`id`, `nom`) VALUES (NULL, 'C3');
INSERT INTO `competence` (`id`, `nom`) VALUES (NULL, 'C4');
INSERT INTO `competence` (`id`, `nom`) VALUES (NULL, 'C5');
INSERT INTO `competence` (`id`, `nom`) VALUES (NULL, 'C6');
INSERT INTO `competence` (`id`, `nom`) VALUES (NULL, 'C7');
INSERT INTO `competence` (`id`, `nom`) VALUES (NULL, 'C8');

INSERT INTO `techno` (`id`, `nom`) VALUES (NULL, 'HTML');
INSERT INTO `techno` (`id`, `nom`) VALUES (NULL, 'CSS');
INSERT INTO `techno` (`id`, `nom`) VALUES (NULL, 'JS');