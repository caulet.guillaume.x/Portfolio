<?php

$host = 'localhost';
$db   = 'portfolio';
$user = 'port_admin';
$pass = 'portadmin';

$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);


function addProjet($titre, $image, $description, $date, $lien, $idTechno,$comps){
    global $pdo;
    $req = $pdo->prepare("INSERT INTO projet (titre, img, desc_p, date_p, lien_p) VALUES (?, ?, ?, ?, ?);");
    $req->execute([$titre, $image, $description, $date, $lien ]);
    /// ajoute les techno
    // recuperer l'id du projet
    $id_projet = $pdo->lastInsertId();
    // recuperer l'id des techno
    // ajouter dans la table p_techno les ids proj et tech
    foreach ($idTechno as $i_tech){
        global $pdo;
        if ($i_tech){
            $req = $pdo->prepare("INSERT INTO p_techno(id_projet, id_techno) VALUES (?,?);");
            $req->execute([$id_projet, $i_tech]);
        }
    }    
    
    foreach ($comps as $id_comp){
        global $pdo;
        if ($id_comp){
            $req = $pdo->prepare("INSERT INTO p_competence(id_projet, id_comp) VALUES (?,?);");
            $req->execute([$id_projet, $id_comp]);
        }
    }
}


// rajouter une categorie 

function addTechno($ajtech){
    global $pdo;
    $req = $pdo->prepare("INSERT INTO techno(nom) VALUES (?);");
    $req->execute([$ajtech]);
}

function getAllTechno(){
    global $pdo;
    $req = $pdo -> query('SELECT * FROM techno');
    return $req ->fetchAll();}

function getAllPreProjet(){
    global $pdo;
    $req = $pdo -> query('SELECT * FROM projet');
    return $req ->fetchAll();
}
    